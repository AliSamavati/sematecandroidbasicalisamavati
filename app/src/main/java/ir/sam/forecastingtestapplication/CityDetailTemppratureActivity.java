package ir.sam.forecastingtestapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class CityDetailTemppratureActivity extends AppCompatActivity {
    ImageView image;
    TextView cityNameTextView;
    TextView cityCurrentTempTextView;
    TextView cityHighTempTextView;
    TextView cityLowTempTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_detail_tempprature);
        final String cityName = getIntent().getStringExtra("city_name");
        final String cityURL = getIntent().getStringExtra("city_url");
        final String[] separated =getIntent().getStringExtra("temp").split("/");
        image=(ImageView) findViewById(R.id.city_image_view);
        cityNameTextView = (TextView) findViewById(R.id.city_name_text_view);
        cityHighTempTextView = (TextView) findViewById(R.id.city_high_temp);
        cityLowTempTextView = (TextView) findViewById(R.id.city_low_temp);
        cityCurrentTempTextView = (TextView) findViewById(R.id.city_current_temp);
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(CityDetailTemppratureActivity.this).load(cityURL).into(image);
                        cityNameTextView.setText(cityName);
                        cityCurrentTempTextView.setText(separated[0]);
                        cityLowTempTextView.setText(separated[1]);
                        cityHighTempTextView.setText(separated[2]);
                    }
                });
            }
        }).start();


    }
}
