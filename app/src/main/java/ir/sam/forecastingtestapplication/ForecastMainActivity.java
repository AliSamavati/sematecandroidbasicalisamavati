package ir.sam.forecastingtestapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ir.sam.forecastingtestapplication.WeatherModel;
import ir.sam.forecastingtestapplication.adapters.CityChooseAdpter;
import ir.sam.forecastingtestapplication.databases.WeatherDBClass;
import ir.sam.forecastingtestapplication.models.CityModel;

public class ForecastMainActivity extends AppCompatActivity {
    GridView gridOfAllCity;
    WeatherDBClass weatherDBClass = new WeatherDBClass(this, "samweather.db", null, 2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast_main);
        gridOfAllCity = (GridView) findViewById(R.id.main_grid_view);
        List<CityModel> cityModels = new ArrayList<CityModel>();
        cityModels.add(new CityModel("تهران", "http://en.tehran.ir/portals/0/Image/1386/51/Tehran-Azadi-00.jpg", "112931"));
        cityModels.add(new CityModel("اصفهان", "http://fazagasht.com/wp-content/uploads/2016/08/The-Si-o-seh-Bridge-Esfahan-Iran.jpg", "418863"));
        cityModels.add(new CityModel("شیراز", "http://en.kooleh.info/Files/Image/shiraz-Sadi-tomb-from-Shutterstock.jpg", "115019"));
        cityModels.add(new CityModel("یزد", "http://iranknowledge.net/wp-content/uploads/2016/08/yazd___amir_chakmak_mosque_by_nightcitylights-d76d8lc.jpg", "111822"));
        cityModels.add(new CityModel("رشت", "http://www.beytoote.com/images/stories/iran/ir3175-7.jpg", "118743"));
        cityModels.add(new CityModel("همدان", "https://kalouttravel.com/uploads/1-hegmatane/boali-sina-tomb-hamedan-1378246872.jpg", "132144"));
        cityModels.add(new CityModel("کرمانشاه", "http://ramanco.co/JiroCMS/Files/DATA/maps/Kermanshah/Kermanshah_13.jpg", "128226"));
        cityModels.add(new CityModel("تبریز", "http://arktourism.ir/wp-content/uploads/2014/05/b20551.jpg", "113646"));
        cityModels.add(new CityModel("بندرعباس", "http://www.iranview.com/content/images/posts/19040_760px.jpg", "141681"));
        cityModels.add(new CityModel("زاهدان", "http://media.mehrnews.com/d/2015/06/28/3/1751168.jpg?ts=1486462047399", "1159301"));
        cityModels.add(new CityModel("یاسوج", "http://www.dana.ir/File/ImageThumb_0_608_458/736614", "132548"));
        cityModels.add(new CityModel("ساری", "http://images.kojaro.com/2016/10/0943bece-084b-4cc5-ba95-0e324a190bfb.jpg", "116996"));


        CityChooseAdpter cityChooseAdpter = new CityChooseAdpter(ForecastMainActivity.this, cityModels);
        gridOfAllCity.setAdapter(cityChooseAdpter);
        gridOfAllCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                final CityModel cityModel = (CityModel) gridOfAllCity.getItemAtPosition(position);
                final WebServiceConnection webServiceConnection = new WebServiceConnection(cityModel.getCityID(), ForecastMainActivity.this);
                final String sendParm = "http://api.openweathermap.org/data/2.5/weather?id=" + cityModel.getCityID() + "&units=metric&appid=875691eff759f748fc0d99dc9cf19b40";
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String retValueFromWebService = webServiceConnection.getWeatherInformation(sendParm, ForecastMainActivity.this);
                            //                     String retValueFromJasonParser = jsonObjectParser(retValueFromWebService);
                            String retValueFromJasonParser = jsonObjectParserWithGson(retValueFromWebService, cityModel.getCityName());

                            if (retValueFromJasonParser == "ERROR") {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(ForecastMainActivity.this, "ERROR IN PARSING JSON", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }).start();
                            } else {
                                Intent cityDetailTemperatureIntent = new Intent(ForecastMainActivity.this, CityDetailTemppratureActivity.class);
                                cityDetailTemperatureIntent.putExtra("city_name", cityModel.getCityName());
                                cityDetailTemperatureIntent.putExtra("city_url", cityModel.getCityImageurl());
                                cityDetailTemperatureIntent.putExtra("temp", retValueFromJasonParser);
                                startActivity(cityDetailTemperatureIntent);
                            }

                        } catch (final Exception e) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ForecastMainActivity.this, "Error in calling webservice " + e, Toast.LENGTH_LONG).show();
                                        }
                                    });

                                }
                            }).start();
                        }
                    }
                }).start();


            }
        });

    }

    public String jsonObjectParser(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            jsonObject = new JSONObject(jsonObject.getString("main"));
            String cTemp = jsonObject.getString("temp");
            String maxTemp = jsonObject.getString("temp_max");
            String minTemp = jsonObject.getString("temp_min");
            return cTemp + "/" + minTemp + "/" + maxTemp;

            // Toast.makeText(ForecastMainActivity.this,cTemp,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            //  Toast.makeText(ForecastMainActivity.this,"Error in parsing JSON "+e,Toast.LENGTH_LONG).show();
            return "ERROR";
        }
    }

    public String jsonObjectParserWithGson(String jsonString, String cityName) {
        Gson gson = new Gson();
        WeatherModel weatherModel = gson.fromJson(jsonString, WeatherModel.class);
        String cTemp = weatherModel.getMain().getTemp().toString();
        String maxTemp = weatherModel.getMain().getTempMax().toString();
        String minTemp = weatherModel.getMain().getTempMin().toString();
        Calendar c = Calendar.getInstance();
    /*    final String seconds = c.get(Calendar.YEAR)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.DATE)+"_"+c.get(Calendar.HOUR)+":"+
                c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND)+"";
*/

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
/*        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ForecastMainActivity.this,formattedDate,Toast.LENGTH_LONG).show();
                    }
                });
            }
        }).start();*/

        weatherDBClass.onInsert(cityName, weatherModel.getId().toString(), weatherModel.getMain().getTempMax(),
                weatherModel.getMain().getTempMin(), weatherModel.getMain().getTemp(), formattedDate);
        return cTemp + "/" + minTemp + "/" + maxTemp;


    }
}
