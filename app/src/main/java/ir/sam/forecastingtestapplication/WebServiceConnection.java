package ir.sam.forecastingtestapplication;

import android.content.Context;
import android.preference.PreferenceActivity;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sam on 5/7/2017.
 */

public class WebServiceConnection {
    String webServiceURL;
    Context mContext;

    public String getWebServiceURL() {
        return webServiceURL;
    }

    public void setWebServiceURL(String webServiceURL) {
        this.webServiceURL = webServiceURL;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public WebServiceConnection(String webServiceURL, Context mContext) {

        this.webServiceURL = webServiceURL;
        this.mContext = mContext;
    }

    public String getWeatherInformation(String webServiceURL, Context mContext) throws Exception {

        URL obj = new URL(webServiceURL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            return response.toString();
        } else {
            return  "GET request not worked";
        }


    }
//    private String getWeatherInformationAsyncHttpClient(String webServiceURL) throws Exception {
//        AsyncHttpClient contact_client = new AsyncHttpClient();
//        contact_client.get(webServiceURL, null, new TextHttpResponseHandler() {
//            @Override
//            public String onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                return "Error in getting file";
//            }
//
//            @Override
//            public String onSuccess(int statusCode, Header[] headers, String responseString) {
//                return responseString;
//            }
//        }
//
//
//    }
}

