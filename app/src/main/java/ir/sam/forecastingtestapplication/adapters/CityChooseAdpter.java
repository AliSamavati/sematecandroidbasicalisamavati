package ir.sam.forecastingtestapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.sam.forecastingtestapplication.ForecastMainActivity;
import ir.sam.forecastingtestapplication.R;
import ir.sam.forecastingtestapplication.models.CityModel;

/**
 * Created by sam on 5/3/2017.
 */

public class CityChooseAdpter extends BaseAdapter {
    Context mContext;
    List<CityModel> cityModel;

    public CityChooseAdpter(Context mContext, List<CityModel> cityModel) {
        this.mContext = mContext;
        this.cityModel = cityModel;
    }

    @Override
    public int getCount() {
        return cityModel.size();
    }

    @Override
    public Object getItem(int position) {
        return cityModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View itemView= LayoutInflater.from(mContext).inflate(R.layout.grid_of_all_city,viewGroup,false);
        ImageView image= (ImageView) itemView.findViewById(R.id.imageViewForGridOfAllCity);
        Glide.with(mContext).load(cityModel.get(position).getCityImageurl()).fitCenter().into(image);
        TextView textView=(TextView) itemView.findViewById(R.id.textViewForGridAllCity);
        textView.setText(cityModel.get(position).getCityName());
        return itemView;
    }
}
