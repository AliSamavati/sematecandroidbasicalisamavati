package ir.sam.forecastingtestapplication.databases;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by sam on 5/13/2017.
 */

public class WeatherDBClass extends SQLiteOpenHelper {
    String createTableStatement=""+
            "CREATE TABLE weather_status ("+
            "_id INTEGER PRIMARY KEY ,"+
            "city_name TEXT ,"+
            "city_id TEXT ,"+
            "high_temp INTEGER ,low_temp INTEGER ,current_temp INTEGER )"+
            "";
    String alterTableStatement1=""+
            "ALTER TABLE weather_status ADD COLUMN current_timestamp TEXT ";
    public WeatherDBClass(Context context, String name, SQLiteDatabase.CursorFactory factory,  int version) {
        super(context, name, factory, version);
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createTableStatement);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int version) {
        sqLiteDatabase.execSQL(alterTableStatement1);

    }
    public void onInsert(String cityName, String cityID, int highTemp, int lowTemp, double currentTemp,String dateTime){
        String insertQuery="INSERT INTO weather_status "+
                "(city_name,city_id,high_temp,low_temp,current_temp,current_timestamp) "+
                "VALUES ( '"+cityName+"' ,'"+cityID+"',"+highTemp+","+lowTemp+","+currentTemp+",'"+dateTime+"' )";
        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        sqLiteDatabase.execSQL(insertQuery);
        sqLiteDatabase.close();

    }
}
