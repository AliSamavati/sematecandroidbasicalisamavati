package ir.sam.forecastingtestapplication.models;

/**
 * Created by sam on 5/3/2017.
 */

public class CityModel {
    String cityName;

    String cityImageurl;
    String cityID;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityImageurl() {
        return cityImageurl;
    }

    public void setCityImageurl(String cityImageurl) {
        this.cityImageurl = cityImageurl;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public CityModel(String cityName, String cityImageurl,String cityID) {

        this.cityName = cityName;
        this.cityImageurl = cityImageurl;
        this.cityID=cityID;
    }
}