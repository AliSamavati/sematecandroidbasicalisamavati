
package ir.sam.forecastingtestapplication ;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherModel {

    @SerializedName("coord")
    @Expose
    private ir.sam.forecastingtestapplication.Coord coord;
    @SerializedName("weather")
    @Expose
    private List<ir.sam.forecastingtestapplication.Weather> weather = null;
    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("main")
    @Expose
    private ir.sam.forecastingtestapplication.Main main;
    @SerializedName("visibility")
    @Expose
    private Integer visibility;
    @SerializedName("wind")
    @Expose
    private ir.sam.forecastingtestapplication.Wind wind;
    @SerializedName("clouds")
    @Expose
    private ir.sam.forecastingtestapplication.Clouds clouds;
    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("sys")
    @Expose
    private ir.sam.forecastingtestapplication.Sys sys;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cod")
    @Expose
    private Integer cod;

    public ir.sam.forecastingtestapplication.Coord getCoord() {
        return coord;
    }

    public void setCoord(ir.sam.forecastingtestapplication.Coord coord) {
        this.coord = coord;
    }

    public List<ir.sam.forecastingtestapplication.Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<ir.sam.forecastingtestapplication.Weather> weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public ir.sam.forecastingtestapplication.Main getMain() {
        return main;
    }

    public void setMain(ir.sam.forecastingtestapplication.Main main) {
        this.main = main;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public ir.sam.forecastingtestapplication.Wind getWind() {
        return wind;
    }

    public void setWind(ir.sam.forecastingtestapplication.Wind wind) {
        this.wind = wind;
    }

    public ir.sam.forecastingtestapplication.Clouds getClouds() {
        return clouds;
    }

    public void setClouds(ir.sam.forecastingtestapplication.Clouds clouds) {
        this.clouds = clouds;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public ir.sam.forecastingtestapplication.Sys getSys() {
        return sys;
    }

    public void setSys(ir.sam.forecastingtestapplication.Sys sys) {
        this.sys = sys;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

}
